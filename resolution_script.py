#!/usr/bin/env python3

import sys             # library for system related functions
import logging         # library to prin interpretable log messages
import numpy as np     # library to process numeric data
import pandas as pd    # library to read, organize and manipulate data
from fuzzywuzzy import fuzz # string matching library using Levenshtein Distance 
import multiprocessing as mp # library for parallel processing to speed things up
from functools import partial # library to create partial functions


def txt_compare(row, txt_ref):
    """Text comparison function.
    
    Helper function to obtain the string matching result. Uses `fuzzywuzzy` library
    to perform two types of Levenshtein Distances viz., 
    (a) partial string similarity measure and
    (b) tokenize the strings and build comparison strings as intersection and remainder.
    The final output of the function is the average of the two measures.
    
    Parameters
    -----------
    row : dataframe row
        row of CSV to compare
    txt_ref : string
        formatted string to compare against
    
    Returns
    --------
    float
        mathcing score (0 on any errors)
    
    See Also
    --------
    fuzz.token_set_ratio, fuzz.partial_ratio
    """
    txt_cmp = '{}. {}. {}. {:.0f}.'.format(row['title'], row['authors'], row['venue'], row['year'])

    try:
        pp = fuzz.token_set_ratio(txt_cmp, txt_ref) 
        pp += fuzz.partial_ratio(txt_cmp, txt_ref)
    except Exception as e:
        return 0
    else:
        return pp * 0.5
    

def approach3(df1, df2, idx=0, threshold=3):
    """Approach# 3 as described in the report.
    
    This approach uses the function `txt_compare` on a single indexed entry of `df1`
    and compares with every row in `df2`. It uses multi-processing library to paralellize 
    the operation. 
    
    Once all the rows of `df2` are scored, the maximum value and standard deviation
    of the scores is obtained. The acceptable range is defined as:
    
    .. math:: 
    
        \text{acceptable range} = \left[\text{maximum}, \text{maximum}-\text{threshold}\times\text{std. dev}\right]
    
    
    Parameters
    -----------
    df1: pd.DataFrame
        data frame to compare
    df2: pd.DataFrame
        data frame to compare against
    idx: int
        index of row in `df1` to compare
    threshold: int
        specify acceptable range of measures 
    
    Returns
    --------
    pd.Dataframe
        mathched rows of df2
        
    See Also
    --------
    txt_compare
    """

    title = df1.title.values[idx]
    author = df1.authors.values[idx]
    venue = df1.venue.values[idx]
    year = df1.year.values[idx]
    txt_reference = '{}. {}. {}. {:.0f}.'.format(title, author, venue, year)
    logging.debug(txt_reference)

    with mp.Pool(mp.cpu_count()) as pool:
        # Create a pool of worker threads and distribute the job
        p = pool.map(
            partial(txt_compare, txt_ref=txt_reference), 
            [row for _, row in df2.iterrows()],
            chunksize=1000
        )

    p = np.array(p)
    logging.debug('max: {:.2f}\t std: {:.2f}\t threshold: {:.2f}'.format(p.max(), p.std(), p.max()-p.std()*threshold))

    return df2.loc[(p >= (p.max() - (threshold*p.std()) )) & (p.max() > 80),:]


def main():
    """
    Wrapper for main function similar to "C"
    (This approach helps in keeping script readable.)
    
    As a measure for optimization, the rows in df2 that are found as 
    matches are then dropped for subsequent iterations.
    
    See Also
    ---------
    approach3
    """

    # Step 1: Read input CSV files
    try: 
        dblp = pd.read_csv('data_in/DBLP1.csv', encoding='iso8859_15', header=0, engine='python')
        scholar = pd.read_csv('data_in/Scholar.csv', encoding='iso8859_15', engine='python')
    except Exception as e:
        logging.error(e)
        return 1
    
    dblp_scholar = pd.DataFrame(columns=['idDBLP', 'idScholar', 'DBLP_Match', 'Scholar_Match', 'Match_ID'])

    for idx in range(dblp.shape[0]):
        dblp_row = dblp.loc[idx, ['idDBLP', 'Row_ID']]
        dblp_row = pd.DataFrame(dblp_row).transpose()
        dblp_row.rename(columns={'Row_ID': 'DBLP_Match'}, inplace=True)

        sch_match = approach3(dblp, scholar, idx=idx)
        if sch_match.empty:
            continue
        sch_match = sch_match.drop(columns=['title','authors','venue','year'])
        sch_match.rename(columns={'ROW_ID': 'Scholar_Match'}, inplace=True)

        for sch_idx, sch_match_row in sch_match.iterrows():
            dblp_scholar = dblp_scholar.append([{
                'idDBLP'        : dblp_row['idDBLP'].values[0], 
                'idScholar'     : sch_match_row['idScholar'], 
                'DBLP_Match'    : dblp_row['DBLP_Match'].values[0], 
                'Scholar_Match' : sch_match_row['Scholar_Match'],
                'Match_ID'      : '{}_{}'.format(dblp_row['DBLP_Match'].values[0], sch_match_row['Scholar_Match'])
            }], ignore_index=True)
            scholar.drop(index=[sch_idx], inplace=True) # optimization to reduce number of rows on subsequent searches
    
    dblp_scholar.to_csv('DBLP_Scholar_perfectMapping_KrishnaVedala.csv', index=False)
    
    return 0
    
    
if __name__ == "__main__":
    # setup logging configuration
    logging.basicConfig(format='%(asctime)s.%(msecs)03d; %(levelname)s; %(funcName)s:%(lineno)d; %(message)s', 
                        level=logging.ERROR, datefmt='%Y-%m-%d %H:%M:%S')
    
    sys.exit(main())
