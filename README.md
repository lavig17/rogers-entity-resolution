# Assignemnt for Rogers - Entity Resolution

## Objective
Given two datasets `Scholar.csv` and `DBLP.csv`, identify and match records
across the datasets and identify as the same entity. Create a final dataset
containing the matched entries while addressing the poor quality of input 
datasets.

## Description of the approach
### The datasets
The two datasets contain the same column names and identify the publication 
title, author, venue of publication and the year published. However, the 
datasets are very noisy for the following reasons:
1. entries may have one or more fields missing
2. entries may have one or more fields containing spelling errors
3. entries may have character contamination i.e., characters might be misplaced
4. Other unknown/unidentifiable errors

### Assumptions made
1. Entity matching should have **all** columns identical.
2. If one or more columns have incomplete or unknown data, best approximation for
should be made based on available information.
3. Only matched records will be stored in the output CSV.
4. No duplicate entries in DBLP.csv dataset &mdash; if any, they will get
eliminated from the output because of absence of any matches.

### <a name="algorithm"></a>The algorithm
The approach that tends to address the above issues in a semi-autonomous process
is as follows:
1. One of the datasets (the `DBLP.csv` in my case) is used as the iterator for 
rows while the other (the `Scholar.csv`) is used to compare against for matches.
2. For each row of DBLP, a concatenated string is created using 'title', 
'author', 'venue' and 'year'. The same is created for every row of Scholar for
comparison.
3. We obtain a modified score of [Levenshtein Distances](https://en.wikipedia.org/wiki/Levenshtein_distance)
to compare against every row of Scholar. 
  1. obtain *partial ratio*: this is the score of best matching substring
  2. obtain *token set ratio*: tokenize the two strings, split into two groups 
     (viz., intersection and remainder) and build comparison strings
  3. the average of the two scores is an intuitive score for the comparison 
     and the problem at hand.
4. This minimum score for correct matching by empirical analysis, was found to 
be 80 and the acceptable matches are 3 standard deviations from the maximum. 
This approach thus automates the selection and filtering process.
5. This loop is executed for each of the entries in the DBLP dataset and every 
successful match in Scholar is removed from successive searches. This has the 
added benefit that duplicates in the DBLP get automatically eliminated due to
absence of any more matches in Scholar.

### Other approaches
There were two other simpler approaches that were tested to address the problem.
The details can be observed in the [notebook](Entity Resolution.ipynb) and in the corresponding [report](Report.html).
#### Approach 1 &mdash; Probability of matching words
This is a fairly straight forward approach in which individual words are extracted 
from the title to be searched. Words that are two characters or smaller are 
rejected. The remaining words are used to compute the probability of existence in 
the other dataframe. The probability is computed as:
$$P = \frac{\text{number of matches observed}}{\text{number of words searched}}$$
Since all the words should match, we will set a threshold of `1.0` to accept the 
matches. This approach is very powerful but cannot withstand the poor quality of 
the datasets.

#### Approach 2 &mdash; Probability of matching words using NLP
This is a natural extension of the probability of words matching and utilizes
the well-known [Levenshtein Distances](https://en.wikipedia.org/wiki/Levenshtein_distance)
metric to obtain match scores. This approach is very flexible but relies on 
manually setting the range of acceptable score values. I will attempt to follow
the wise words of Bill Gates,
> The first rule of any technology used in a business is that automation 
applied to an efficient operation will magnify the efficiency.

And apply automation towards selecting this range using statistical principles. 
This results in the third approach presented in the [algorithm above](#algorithm). 


## Implementation
Implementation is done in [python 3](https://en.wikipedia.org/wiki/Python_%28programming_language%29).
The testing and development of the approaches was done using Jupyter Notebook. 
The [notebook](Entity Resolution.ipynb) is also made available for view. The 
notebook contains detailed descriptions and graphs for explanations. However, for 
convenience, the notebook was converted to an [independent HTML file](Report.html) 
that can be viewed directly. 

Once the notebook was completed satisfactorily, the code snippets were arranged 
into an independently executable [python script](resolution_script.py). The 
script can be executed directly from a bash terminal as
```
$ ./resolution_script.py
```
or from a windows command line as
```
C:\rogers-entity-resolution> python resolution_script.py
```

### Modules/Libraries used
#### sys
This module is a part of [python standard library](https://docs.python.org/3/library/sys.html) 
and is useful to perform communications with the host system. The script uses 
the `sys.exit` function to report type of script termination i.e., execution 
completed with or without error(s).

### logging
This module is also a part of [python standard library](https://docs.python.org/3/library/logging.html) 
and is useful logging events with flexibility and ease.

### numpy
This is the most powerful [numerical processing](http://www.numpy.org) library
in python. This is being used to perform statistical operations on the array of 
match scores.

### pandas
[Pandas](http://www.numpy.org) is a widely adopted data wrangling library and 
provides with many easy-to-use functions for data manipulation. this is 
extensively used in the code for reading input CSV files and to write output CSV. 
It is also used to search and access data and perform vectorized operations.

### fuzzywuzzy
[This](http://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/)
is a powerful set of string matching routines based on Levenshtein Distance metric.
We use two of its functions as described above and obtain a metric for match 
identification. 

### multiprocessing
This module is also a part of [python standard library](https://docs.python.org/3/library/multiprocessing.html) 
and is useful for parallelizing executions on multi-core systems. 

### functools
This module is also a part of [python standard library](https://docs.python.org/3/library/functools.html) 
and is useful to create partial functions i.e., functions based on other functions

## Known issues and ToDo
### Execution time
The script takes a long time to execute for the complete dataset averaging about 
30sec per row of DBLP. This is because of the selection of comparing "DBLP" (with 
$2616$ rows) with "Scholar" (with $> 60000$ rows). If this operation is reversed, 
the average time goes down to about 600ms per row of Scholar. I chose to compare 
the DBLP with the Scholar because the number of duplicates are much larger in the 
Scholar and hence we will have less chances of missing matches. 

An optimization was added as such &mdash; on every successful match, the rows that 
did match were eliminated from the "Scholar" for further searches. This reduces 
the number of rows to match in successive iterations and hence the speedup. For 
the complete process, the expected time was 21hrs but completed without execution 
errors in ~10hrs.

### Duplicates
The duplicate matches are currently accepted by the script. This is because once 
all the possible matches are obtained, it is better to filter out the unwanted 
ones based on criteria like missing values or spelling mistakes, etc.. Since this 
involves more assumptions to be made, this is left as a future possibility. 

## List of Files/Folders
1. [data_in](data_in) &mdash; folder containing raw input CSV files that were 
shared in the email. The final script reads files from this folder.
2. [email](email) &mdash; folder containing files shared in the assignment email
3. [.gitignore](.gitignore) &mdash; file spcifying files not to track
4. [DBLP_Scholar_perfectMapping_KrishnaVedala.csv](DBLP_Scholar_perfectMapping_KrishnaVedala.csv) 
&mdash; final script output CSV as requested.
5. [Entity Resolution.ipynb](Entity Resolution.ipynb) &mdash; notebook created to 
study and develop the solution to the assignment.
6. [README.md](README.md) &mdash; The current file.
7. [Report](Report) &mdash; Notebook in markdown and PDF formats for convenience.
8. [Report.html](Report.html) &mdash; Notebook converted to easily viewable and 
independent HTML file. Represents the report for the solution.
9. [resolution_script.py](resolution_script.py) &mdash; Algorithm converted to 
an executable script as requested. This script was used to generate the output 
CSV file.
